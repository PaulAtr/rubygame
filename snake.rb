require 'ruby2d'

set background: 'silver'
set fps_cap: 10

GRID_SIZE = 20
GRID_WIDTH = Window.width / GRID_SIZE
GRID_HEIGHT = Window.height / GRID_SIZE

class Snake
    attr_writer :direction

    def initialize
        @positions = [[0,10], [1,10], [2,10], [3,10]]
        @direction = 'right'
        @growing = false
    end

    def draw
        @positions.each do |position|
            Square.new(x: position[0]*GRID_SIZE, y: position[1]*GRID_SIZE, size: GRID_SIZE - 2, color: 'blue')
        end
    end

    def move
        if !@growing
        @positions.shift
        end
        case @direction
        when 'down'
            @positions.push(new_coords(head[0], head[1] +1))
        when 'up'
            @positions.push(new_coords(head[0], head[1] -1))
        when 'left'
            @positions.push(new_coords(head[0]-1, head[1]))
        when 'right'
            @positions.push(new_coords(head[0]+1, head[1]))
        end
        @growing = false
    end

    def change_direction(new_direction)
        case @direction
        when 'up' then new_direction != 'down'
        when 'down' then new_direction != 'up'
        when 'left' then new_direction != 'right'
        when 'right' then new_direction != 'left'
        end
    end

    def x 
        head[0]
    end

    def y 
        head[1]
    end

    def grow
        @growing = true
    end

    def eats_itself
        @positions.uniq.length != @positions.length
    end

    private

    def new_coords(x, y)
        [x % GRID_WIDTH, y % GRID_HEIGHT]
    end

    def head
        @positions.last
    end

end

class Game
    def initialize
        @score = 0
        @food_x = rand(GRID_WIDTH)
        @food_y = rand(GRID_HEIGHT)
        @finished = false
    end

    def draw
        unless g_finished
        Square.new(x: @food_x*GRID_SIZE, y: @food_y*GRID_SIZE, size: GRID_SIZE, color: 'green')
        end
        Text.new("Score: #{@score}", color:"navy", x: 500, y: 15, size: 30)
    end

    def
        snake_eats_food(x, y)
        @food_x == x && @food_y == y
    end

    def food_reachin
        @score +=1
        @food_x = rand(GRID_WIDTH)
        @food_y = rand(GRID_HEIGHT)
    end

    def finish 
        @finished = true
    end

    def g_finished
        @finished
    end
end


snake = Snake.new
game = Game.new

update do 
   clear

   unless game.g_finished
    snake.move
   end
   snake.draw
   game.draw

   if game.snake_eats_food(snake.x, snake.y)
    game.food_reachin
    snake.grow
   end

   if snake.eats_itself
        game.finish        
   end


end

on :key_down do |event|
  if ['up', 'down', 'left', 'right'].include?(event.key)
    if snake.change_direction(event.key)
        snake.direction =event.key
  end
end
end

show